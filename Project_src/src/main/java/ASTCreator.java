public interface ASTCreator {
  AST generateAST(String filepath);
}
