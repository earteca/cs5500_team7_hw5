import antlr.CLexer;
import antlr.CParser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

/**
 * Class that creates C language ASTs.
 */
public class CASTCreator implements ASTCreator {
  /**
   * Generate an AST from the given filepath.
   *
   * @param filepath file to generate AST
   * @return CAST (C language AST) for given file
   */
  public AST generateAST(String filepath) {
    try {
      CharStream input = CharStreams.fromFileName(filepath);
      CLexer lexer = new CLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      CParser parser = new CParser(tokens);
      CParser.GenericAssociationContext inputTree = parser.genericAssociation();
      return new CAST(inputTree);
    } catch (IOException e) {
      //TODO somthing
      return null;
    }
  }
}
