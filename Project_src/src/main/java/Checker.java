/**
 * Root for Checker hierarchy.  Checkers return results from a plagiarism check between two
 * assignments.
 */
public interface Checker {

  //TODO what does the checker return?  What are the results?
  Result check(AST astA, AST astB);

}
