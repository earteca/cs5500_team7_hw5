
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileGen implements FileInterface{

  private String filepath;
  private String[] tempsArray;
  private StringBuilder stringBuilder;
  private String fileExtension;

  public FileGen(String filepath) throws FileNotFoundException, IllegalAccessException {
    stringBuilder = new StringBuilder();
    int index;
    index = filepath.lastIndexOf(".");
    if (index != 0) {
      fileExtension = filepath.substring(index + 1);

    }
    System.out.println("The file index is " + fileExtension);
    if(fileExtension.equals("py")|| fileExtension.equals("c")){
      Scanner inFile1 = new Scanner( new File(filepath)).useDelimiter(" ");
      // List<String> tokens = new ArrayList();
      while (inFile1.hasNext()) {
        //   tokens.add(inFile1.nextLine());
        stringBuilder.append(inFile1.nextLine());

      }
    }
    else {
      throw new IllegalAccessException("Only C or Python files allowed");
    }


    //String[] tempsArray = tokens.toArray(new String[0]);


  }


  @Override
  public String getFileText() {
    return stringBuilder.toString();

  }

  @Override
  public String getFileExtension(){
    return fileExtension;
  }

  @Override
  public boolean sameExtension(FileInterface other){
    if(this.fileExtension.equals(other.getFileExtension())){
      return true;
    }
    else{
return false;    }
  }


}
