public interface FileInterface {
  String getFileText();
  String getFileExtension();
  boolean sameExtension(FileInterface other);
}
