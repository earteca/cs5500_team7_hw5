import java.io.FileNotFoundException;

public interface IModel {
  /**
   * Set the filepath for assignment A.
   *
   * @param filepathA filepath for assignment A
   */
  void setFilepathA(String filepathA) throws FileNotFoundException, IllegalAccessException;
  /**
   * Set the filepath for assignment B.
   *
   * @param filepathB filepath for assignment B
   */
  void setFilepathB(String filepathB) throws FileNotFoundException, IllegalAccessException;
  /**
   * Run a check on the two source assignments.  This method throws errors if
   */
  void runCheck();
  //TODO how to we get the results?
}
