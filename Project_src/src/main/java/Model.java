import java.io.FileNotFoundException;
import java.nio.file.InvalidPathException;
//import java.nio.file.Paths;

/**
 * Class representing the Model for the Plagiarism Checker.  The model holds all of the data and
 * methods to manipulate the data.  The model has a controller that drives method calls for the
 * model.
 */
public class Model implements IModel {
  /**
   * Controller that provides data input and drives method calls for this model.
   */
  private Controller controller;
  /**
   * ASTCreator that creates AST's from this model's file paths.
   */
  private ASTCreator astCreator;
  /**
   * AST tree made from assignment A.
   */
  //TODO can change this to a list of AST's for multiple files?
  private AST sourceATree;
  /**
   * AST tree made from assignment B.
   */
  //TODO can change this to a list of AST's for multiple files?
  private AST sourceBTree;
  /**
   * Checker that runs the checking algorithm on the two ASTs.
   */
  private Checker checker;

  //TODO how will we store the results from the checker?
  private Result result;
private String filepathA;
private String filepathB;
  private FileGen fileA;
  private FileGen fileB;

  /**
   * Construct a model with a given controller.
   *
   * @param controller to provide data input and drive method calls for the created model.
   */
  public Model(Controller controller) {
    this.controller = controller;
  }

  @Override
  public void setFilepathA(String filepathA) throws FileNotFoundException, IllegalAccessException {
    this.filepathA= filepathA;
    fileA= new FileGen(filepathA);
  }

  @Override
  public void setFilepathB(String filepathB) throws FileNotFoundException, IllegalAccessException {
    this.filepathB= filepathB;

    fileB= new FileGen(filepathB);

  }

  @Override
  public void runCheck()throws IllegalArgumentException {
    // Check that filepathA and filepath B are both set, if not send error message
//    if (!bothFilesSet()) {
//      throw new IllegalStateException("Both file paths must be set  and valid to run check.");
    /**
     * add this functionality
     */
//    }
    // Check that filepathA and filepathB both have the same extension, if not send error message
    if (!fileA.sameExtension(fileB)) {
      throw new IllegalStateException("All files must have the same extension.");
    }
    // Check if files are Python, if so create PythonASTCreator and PythonChecker
    if (fileA.getFileExtension().equals("py")) {
      this.astCreator = new PythonASTCreator();
      this.checker = new PythonChecker();
    }
    //        if files are in C, if so create CASTCreator and C_Checker
    else if (fileA.getFileExtension().equals("c")) {
      this.astCreator = new CASTCreator();
      this.checker = new C_Checker();
    }
    //        else, send error message

    // Make AST for file A and set as sourceATree
    this.sourceATree = this.astCreator.generateAST(filepathA);
    // Make AST for file B and set as sourceBTree
    this.sourceBTree = this.astCreator.generateAST(filepathB);
    // Send the AST's to the checker
    this.checker.check(this.sourceATree, this.sourceBTree);
  }

  //private boolean bothFilesSet() {
    // Return true if there are valid files for both A and B
//    try {
//      Paths.get(filepathA);
//    } catch (InvalidPathException  NullPointerException ex) {
//      return false;
//    }
//    try {
//      Paths.get(filepathB);
//    } catch (InvalidPathException | NullPointerException ex) {
//      return false;
//    }
//    return true;
    // }

    private boolean isSameExtension () {
      // Return true if the files have the same extension?
      return false;
    }

    private String getLanguage () {
      // Return the language of the files, i.e. either Python or C
      return "";
    }
  }

