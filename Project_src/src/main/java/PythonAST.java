import antlr.Python3Parser;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

public class PythonAST implements AST {
  private Python3Parser.File_inputContext inputTree;

  public PythonAST(Python3Parser.File_inputContext inputTree) {
    this.inputTree = inputTree;
  }

  @Override
  public String getASTString() {
    RuleContext context = inputTree.getRuleContext();
    AstString astString = new AstString();
    return astString.print(context);
  }




    private class AstString {
    private String string="";

      private boolean ignoringWrappers = true;

      public void setIgnoringWrappers(boolean ignoringWrappers) {
        this.ignoringWrappers = ignoringWrappers;
      }

      public String print(RuleContext ctx) {



        return explore(ctx,  0);
      }

      private String explore(RuleContext ctx, int indentation) {

        boolean toBeIgnored = ignoringWrappers
                && ctx.getChildCount() == 1
                && ctx.getChild(0) instanceof ParserRuleContext;
        if (!toBeIgnored) {
          String ruleName = Python3Parser.ruleNames[ctx.getRuleIndex()];
          for (int i = 0; i < indentation; i++) {
            this.string += "  ";

          }

          this.string += ruleName+"\n";

        }
        for (int i = 0; i < ctx.getChildCount(); i++) {
          ParseTree element = ctx.getChild(i);
          if (element instanceof RuleContext) {
            explore((RuleContext) element,indentation + (toBeIgnored ? 0 : 1));
          }
        }
        return this.string;
      }
    }
  }

