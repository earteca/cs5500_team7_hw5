import antlr.Python3Lexer;
import antlr.Python3Parser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

/**
 * Class that creates PythonASTs.
 */
public class PythonASTCreator implements ASTCreator {

  /**
   * Generate an AST from the given filepath.
   *
   * @param filepath file to generate AST
   * @return PythonAST for given file
   */
  public AST generateAST(String filepath) {
    try {
      CharStream input = CharStreams.fromFileName(filepath);
      Python3Lexer lexer = new Python3Lexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      Python3Parser parser = new Python3Parser(tokens);
      Python3Parser.File_inputContext inputTree = parser.file_input();
      return new PythonAST(inputTree);


    } catch (IOException e) {
      //TODO something
      return null;
    }
  }
}
