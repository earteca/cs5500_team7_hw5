/**
 * Checker for files written in Python.
 */
public class PythonChecker implements Checker {
  @Override
  public Result check(AST astA, AST astB) {
    System.out.println(astA.toString());
    System.out.println("\n\n");
    System.out.println(astB.toString());
    if (astA.toString().equals(astB.toString())) {
      System.out.println("SAME FILE CONTENTS!");
    } else {
      System.out.println("Not the same file contents...");
    }
    return null;
  }
}
