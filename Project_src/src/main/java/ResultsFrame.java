import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ResultsFrame {
  public ResultsFrame() {

  }

  public void createFrame() {
    JFrame resultsframe = new JFrame("Results");
    resultsframe.setSize(400, 500);
    resultsframe.setVisible(true);
    resultsframe.setLayout(null);

    ActionListener viewStats = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        StatsView statsView = new StatsView();
        statsView.createStatFrame();
      }
    };

    ActionListener viewCode = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        CodeView codeView = new CodeView();
        codeView.createCodeView();
      }
    };

    JButton stats = new JButton("Stats View");
    resultsframe.add(stats);
    stats.setVisible(true);
    stats.setBounds(200, 200, 200,100);
    stats.addActionListener(viewStats);

    JButton code = new JButton("Code View");
    resultsframe.add(code);
    code.setVisible(true);
    code.setBounds(0, 200, 200,100);
    code.addActionListener(viewCode);

  }

}
