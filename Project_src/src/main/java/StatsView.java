import javax.swing.*;

public class StatsView {

  public void createStatFrame() {
    JFrame statsframe = new JFrame("Percent similar");
    statsframe.setSize(400, 500);
    statsframe.setVisible(true);
    statsframe.setLayout(null);


    JLabel label = new JLabel("Percent similar: 100%");
    statsframe.add(label);
    label.setBounds(150,200, 200, 100);
    label.setVisible(true);

  }

}
