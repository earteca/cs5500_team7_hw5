import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;


public class View {


  public static void main(String[] args) {
    final JFrame frame = new JFrame("Plagiarism Checker");
    frame.setSize(400, 500);
    frame.setVisible(true);
    frame.setLayout(null);
    JButton b = new JButton("Select File");
    frame.add(b);
    b.setVisible(true);
    b.setBounds(0, 150, 100, 100);



    JButton b1 = new JButton("Select File");
    frame.add(b1);
    b1.setVisible(true);
    b1.setBounds(0, 300, 100, 100);

    JButton run = new JButton("Execute program");
    frame.add(run);
    run.setVisible(true);
    run.setBounds(150, 300, 200,100);
    ActionListener execute = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.out.println("Running program...");
        ResultsFrame resutts = new ResultsFrame();
        resutts.createFrame();
      }
    };

    run.addActionListener(execute);



    final JFileChooser fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        fileChooser.showOpenDialog(frame);
        File selectedFile = fileChooser.getSelectedFile();
        System.out.println("Selected file: " + selectedFile.getAbsolutePath());
      }
    };


    b.addActionListener(actionListener);

    b1.addActionListener(actionListener);


  }

  }
