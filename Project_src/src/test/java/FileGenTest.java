import static org.junit.Assert.*;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;


public class FileGenTest {


    @Test
    public void test() throws FileNotFoundException, IllegalAccessException {
      FileInterface file= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demo1.py");
      System.out.println(file.getFileText());
      assertEquals(file.getFileExtension(),"py");

    }
  @Test
  public void test2() throws FileNotFoundException, IllegalAccessException {
    FileInterface file= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demoC.c");
    System.out.println(file.getFileText());
    assertEquals(file.getFileExtension(),"c");

  }
  @Test(expected = IllegalAccessException.class)
  public void test3() throws FileNotFoundException, IllegalAccessException {
    FileInterface file= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/texter.txt");
    System.out.println(file.getFileText());

  }

  @Test
  public void test4() throws FileNotFoundException, IllegalAccessException {
    FileInterface file= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demoC.c");
    FileGen file1= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demoC2.c");
    FileGen file3= new FileGen("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demo1.py");


    System.out.println(file.getFileText());
    assertEquals(file.getFileExtension(),"c");
    assertEquals(file.sameExtension(file1), true);
 //   assertEquals(file.sameExtension(file2), false);
    assertEquals(file.sameExtension(file3), false);


  }


  }
