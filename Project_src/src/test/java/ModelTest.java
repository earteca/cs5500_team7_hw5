import org.junit.Test;

import java.io.FileNotFoundException;


import static org.junit.Assert.*;

public class ModelTest {

  @Test
  public void test() throws FileNotFoundException, IllegalAccessException {
    Controller ob= new Controller();
    Model model1= new Model(ob);
    model1.setFilepathA("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demo1.py");
    model1.setFilepathB("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/pythonFile.py");
    model1.runCheck();


  }

  @Test
  public void testSameFiles() throws FileNotFoundException, IllegalAccessException{
    Controller ob= new Controller();
    Model model1= new Model(ob);
    model1.setFilepathA("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demo1.py");
    model1.setFilepathB("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/demo1.py");
    model1.runCheck();
  }

}