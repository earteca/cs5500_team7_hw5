import org.junit.Test;

public class PythonASTCreatorTests {

  @Test
  public void createPythonAST() {
    PythonASTCreator creator = new PythonASTCreator();
    //AST ast = creator.generateAST("/Users/apriljoy/Desktop/FSD 5500/Team-07-F19/Project_src/src/test/java/test.py");
    AST ast = creator.generateAST("/Users/Jake/Documents/5500/Project/Team-07-F19/Project_src/src/test/java/test.py");
    String str = ast.getASTString();
    System.out.println("\n\n\n");
    System.out.println(str);
  }

}
