import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;



public class Pytest {
  public static void main(String[] args) throws Exception {



    //ANTLRInputStream input = new ANTLRInputStream(inputStream);
    //System.out.println(input.toString());

    //CharStream input = CharStreams.fromFileName("pythontest.py");
    CharStream input = CharStreams.fromFileName("/Users/Jake/Documents/5500/Project/Team-07-F19/antlrtest/src/main/java/pythontest.py");

    Python3Lexer lexer = new Python3Lexer(input);
    System.out.println(lexer.toString());

    CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.toString());

    Python3Parser parser = new Python3Parser(tokens);
    System.out.println(parser.toString());

    Python3Parser.File_inputContext tree = parser.file_input();

    Python3Listener listener = new Python3BaseListener();
    ParseTreeWalker.DEFAULT.walk(listener, tree);

    RuleContext ctx = tree.getRuleContext();
    AstPrinter.print(ctx);



    //System.out.println(tree.toStringTree(parser));



  }
  public static class AstPrinter {
    public static void print(RuleContext ctx) {
      explore(ctx, 0);
    }
    private static void explore(RuleContext ctx, int indentation) {
      String ruleName = Python3Parser.ruleNames[ctx.getRuleIndex()];
      for (int i=0;i<indentation;i++) {
        System.out.print("  ");
      }
      System.out.println(ruleName);
      for (int i=0;i<ctx.getChildCount();i++) {
        ParseTree element = ctx.getChild(i);
        if (element instanceof RuleContext) {
          explore((RuleContext)element, indentation + 1);
        }
      }
    }

  }
}
