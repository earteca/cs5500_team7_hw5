class Stack:
    def __init__(self):
        self.mystack = []

    def push(self, element):
        self.mystack.append(element)

    def pop(self):
        self.mystack.pop()

    def is_empty(self):
        return len(self.mystack) == 0

    def top(self):
        if not self.is_empty():
            return self.mystack[-1]
        return None
