package ui;

import java.awt.event.MouseEvent;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.event.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.text.Text;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a file");

        Button button = new Button("Choose a file");
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                fileChooser.showOpenDialog(primaryStage);
            }
        };


        Button button2 = new Button("Choose a second file");
        EventHandler<ActionEvent> event1 = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                fileChooser.showOpenDialog(primaryStage);
            }
        };
        button.setOnAction(event);
        button2.setOnAction(event1);

        VBox vBox = new VBox();
        Text prompt1 = new Text("Choose the first assignment");
        vBox.getChildren().add(prompt1);
        vBox.getChildren().add(button);
        vBox.setSpacing(25);
        Text prompt2 = new Text("Choose the second assignment");
        vBox.getChildren().add(prompt2);
        vBox.getChildren().add(button2);



        primaryStage.setTitle("Plagiarism Checker");
        primaryStage.setScene(new Scene(vBox, 750, 750));

        primaryStage.show();




    }


    public static void main(String[] args) {
        launch(args);
    }
}
